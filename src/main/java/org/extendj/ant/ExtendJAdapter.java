/* Copyright (c) 2018, Jesper Öqvist
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 * this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 * this list of conditions and the following disclaimer in the documentation
 * and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 * contributors may be used to endorse or promote products derived from this
 * software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.extendj.ant;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.net.URLClassLoader;

import org.apache.tools.ant.taskdefs.compilers.DefaultCompilerAdapter;
import org.apache.tools.ant.taskdefs.Javac;
import org.apache.tools.ant.BuildException;
import org.apache.tools.ant.Project;
import org.apache.tools.ant.Location;
import org.apache.tools.ant.types.Commandline;
import org.apache.tools.ant.types.Path;
import org.apache.tools.ant.types.FileSet;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

public class ExtendJAdapter extends DefaultCompilerAdapter {
  private static final String EXTENDJ_PATH = "extendj.jar";
  private static final String EXTENDJ_LOG = "extendj.log";

  @Override
  public boolean execute() throws BuildException {
    String extendjPath = project.getProperty(EXTENDJ_PATH);
    String extendjLog = project.getProperty(EXTENDJ_LOG);
    extendjLog = extendjLog == null ? "" : extendjLog.trim();
    if (extendjPath == "" ||
        extendjPath.isEmpty()
        || !(new File(extendjPath).isFile())) {
      String message = String.format(
          "The %s property must be set to the location of an ExtendJ jar.",
          EXTENDJ_PATH);
      attributes.log(message, Project.MSG_ERR);
      throw new BuildException(message);
    }

    Commandline cmd = setupModernJavacCommand();

    try {
      if (!extendjLog.isEmpty()) {
        File logfile = new File(extendjLog);
        try {
          String message = "Writing compile argument to file: " + logfile.getAbsolutePath();
          attributes.log(message, Project.MSG_INFO);
          FileOutputStream f = new FileOutputStream(logfile);
          PrintStream out = new PrintStream(f);
          for (String arg : cmd.getArguments()) {
            out.println(arg);
          }
          out.close();
          f.close();
        } catch (IOException e) {
          String message = "Failed to write compile argument log: " + logfile.getAbsolutePath();
          attributes.log(message, Project.MSG_WARN);
        }
      }
      File extendjJar = new File(extendjPath);
      URL[] urls = { extendjJar.toURI().toURL() };
      URLClassLoader classLoader = new URLClassLoader(urls);
      boolean pass;
      Class<?> compilerClass = classLoader.loadClass("org.extendj.JavaCompiler");
      Object compiler = compilerClass.getConstructor().newInstance();
      Object result =
          compiler
              .getClass()
              .getMethod("compile", String[].class)
              .invoke(compiler, (Object) new String[] { "-version" });
      pass = (Boolean) result;
      if (pass) {
        result = compiler
            .getClass()
            .getMethod("compile", String[].class)
            .invoke(compiler, (Object) cmd.getArguments());
        pass = (Boolean) result;
      }
      return pass;
    } catch (ClassNotFoundException e) {
      String message = String.format(
          "The ExtendJ class %s was not found! "
          + "Verify that the extendj.jar property points to an ExtendJ Jar file.",
          e.getMessage());
      attributes.log(message, Project.MSG_ERR);
      throw new BuildException(message, location);
    } catch (Exception e) {
      if (e instanceof BuildException) {
        throw (BuildException) e;
      } else {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        String message = sw.toString();
        attributes.log(message, Project.MSG_ERR);
        throw new BuildException("Error running ExtendJ: " + e.getMessage(), e, location);
      }
    }
  }
}
